# 2020 Stem Cell Network RNASeq Workshop

## Workshop Dates
Workshop teaching sessions: July 27-28, 2020 
Tutorial sessions; July 29-30, 2020 


## Workshop Venue

A Microsoft Teams invite will be sent to meeting participants

## Organizers		
Dr. Bill Stanford (uOttawa/OHRI)  
Dr. Ted Perkins (uOttawa/OHRI)  

## Workshop Files

[Workshop Agenda](Files/2020_RNA-Seq_Analysis_Workshop_Agenda.pdf?inline=false) _UPDATED July 27, 2020_

### Day 1:

Workshop and Goals (Dr. Bill Stanford) [PDF](Files/RNA-seq_workshop_Stanford_v1_2020.pdf)

RNA-seq Basics (Gareth Palidwor) [PowerPoint](Files/2020_Workshop_Lecture_1_RNASeq_Basics.pptx)
* Example FASTQC files
    * [ERR975344.1_fastqc](https://www.ogic.ca/projects/workshop_2019/FastQC/ERR975344.1_fastqc.html?inline=false)
    * [ERR975344.2_fastqc](https://www.ogic.ca/projects/workshop_2019/FastQC/ERR975344.2_fastqc.html?inline=false)

Differential Gene Expression with DESeq2 (Chris Porter) [PowerPoint](Files/2020_Workshop_Lecture_2_DESeq2.pptx)
* [Salmon gene counts for Differential Gene Expression Analysis](http://www.ogic.ca/projects/2020_RNASeq_Workshop/RNA-seq_salmon_files.zip)
* [R Script for Differential Gene Expression Analysis](Files/DESeq2_analysis_script.Rmd?inline=false)
* [Ranked significant gene list output g:Profiler exercise](Files/Ensembl_Act_vs_Qui_sig05_salmon.txt?inline=false)
* [Full ranked gene list ouptput for GSEA exercise](Files/Ensembl_Act_vs_Qui_all_salmon.txt?inline=false)

Gene Set Enrichment Analysis (Gareth Palidwor) [PowerPoint](Files/2020_Workshop_Lecture_3_GeneAnnotationEnrichmentAnalysis.ppt)
* [g:Profiler instructions](Files/gprofiler.md)

RNA-seq Experimental Design  (Dr. Ted Perkins) [PDF](Files/Perkins_RNASeqWorkshop_2020.pdf)

### Day 2:

Introduction to the single-cell RNA sequencing workflow (David Cook) [PowerPoint](http://dropbox.ogic.ca/workshop_2020/scRNAseq_workshop_ppt.pptx)
* [filtered feature bc matrix](http://dropbox.ogic.ca/workshop_2020/filtered_feature_bc_matrix/)
* [01_processing.Rmd](Files/01_processing.Rmd)
* [02_downstream_analysis.Rmd](Files/02_downstream_analysis.Rmd)
* [03_trajectory_inference.Rmd](Files/03_trajectory_inference.Rmd)
* [live_scripting.Rmd](Files/live_scripting.Rmd) _supplemental filie generated during workshop by David_



